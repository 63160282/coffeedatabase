/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirarat.coffedatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author User
 */
public class DeleteDatabase11 {
    public static void main(String[] args) {
        Connection conn = null;
         
        try {
            String url = "jdbc:sqlite:D:/sql/dcoffeenew.db";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        //Insert
        String sql = "DELETE FROM catergory WHERE catergory_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            
            int status = stmt.executeUpdate();
            
     //       ResultSet key = stmt.getGeneratedKeys();
     //       key.next();
       //     System.out.println(""+key.getInt(1));
        
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
